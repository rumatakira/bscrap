#!/usr/bin/env php
<?php
// application.php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Dotenv\Dotenv;
use App\Command\MainParserCommand;
use App\Command\ParseTennisCommand;
use App\Command\UnibetTennisWinnerOuGamesCommand;
use App\Command\UnibetTennisOuSetsCommand;
use App\Command\MeridianbetMETennisCommand;
use App\Command\MaxbetMETennisWinnerCommand;
use App\Command\OlimpwinMETennisWinnerCommand;
use App\Command\BusinessLogicTennisCommand;

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/.env');

// You can also load several files
//$dotenv->load(__DIR__.'/.env', __DIR__.'/.env.dev')

$application = new Application();

// ... register commands
$application->add(new MainParserCommand());
$application->add(new ParseTennisCommand());
$application->add(new UnibetTennisWinnerOuGamesCommand());
$application->add(new UnibetTennisOuSetsCommand());
$application->add(new MeridianbetMETennisCommand());
$application->add(new MaxbetMETennisWinnerCommand());
$application->add(new OlimpwinMETennisWinnerCommand());
$application->add(new BusinessLogicTennisCommand());

$application->run();
