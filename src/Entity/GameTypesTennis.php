<?php

namespace App\Entity;

class GameTypesTennis
{
    const GT_TENNIS = [
        'winner',
        'winner_1_set',
        'winner_2_set',
        'total_games_over_under_19.5',
        'total_games_over_under_22.5',
        'total_games_over_under_23.5',
        'odd_even',
        'total_sets_over_under_2.5'
    ];
}
