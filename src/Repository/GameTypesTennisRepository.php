<?php

namespace App\Repository;

use App\Entity\GameTypesTennis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GameTypesTennis|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameTypesTennis|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameTypesTennis[]    findAll()
 * @method GameTypesTennis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameTypesTennisRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GameTypesTennis::class);
    }

    // /**
    //  * @return GameTypesTennis[] Returns an array of GameTypesTennis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameTypesTennis
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrnullResult()
        ;
    }
    */
}
