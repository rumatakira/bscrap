<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190615010402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create {tennis_forks} table in {{bscrap}} database.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tennis_forks ( id INT NOT NULL AUTO_INCREMENT, crated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL default CURRENT_TIMESTAMP, event_date DATE, event_time TIME, host_id INT NOT NULL, host_bookmaker VARCHAR (255) NOT NULL, host VARCHAR (255) NOT NULL, host_odd_under_odd DECIMAL (4, 2) NOT NULL, away_id INT NOT NULL, away_bookmaker VARCHAR (255) NOT NULL, away VARCHAR (255) NOT NULL, away_odd_over_odd DECIMAL (4, 2) NOT NULL, game_type VARCHAR (255) NOT NULL, ou_line NUMERIC(4, 1) DEFAULT NULL, float_fork DECIMAL (3, 3) NOT NULL, percentage_fork DECIMAL (6, 2) NOT NULL, PRIMARY KEY ( id )) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tennis_forks');
    }
}
