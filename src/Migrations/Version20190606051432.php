<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190606051432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create {tennis} table in {{bscrap}} database';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tennis (id INT AUTO_INCREMENT NOT NULL, created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, event_date DATE DEFAULT NULL, event_time TIME DEFAULT NULL, bookmaker VARCHAR(255) NOT NULL, host VARCHAR(255) NOT NULL, host_odd_under_odd NUMERIC(4, 2) NOT NULL, away VARCHAR(255) NOT NULL, away_odd_over_odd NUMERIC(4, 2) NOT NULL, analized TINYINT(1) NOT NULL DEFAULT 0, game_type VARCHAR(255) NOT NULL, ou_line NUMERIC(4, 1) DEFAULT NULL, PRIMARY KEY(id), INDEX (bookmaker), INDEX (host), INDEX (away), INDEX (game_type)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tennis');
    }
}
