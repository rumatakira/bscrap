<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

class MainParserCommand extends Command
{

    protected static $defaultName = 'app:main-parser';

    protected function configure()
    {
        $this
            ->setDescription('Main app command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // parse all tennis sites
        $command = $this->getApplication()->find('app:parse-tennis');
        $arguments = [
            'command' => 'parse-tennis',
        ];
        $greetInput = new ArrayInput($arguments);

        $returnCode = $command->run($greetInput, $output);

        // save tennis forks to the {tennis_forks} table
        $command = $this->getApplication()->find('app:business-logic-tennis');
        $arguments = [
            'command' => 'business-logic-tennis'
        ];
        $inputArray = new ArrayInput($arguments);
        $command->run($inputArray, $output);
    }
}
