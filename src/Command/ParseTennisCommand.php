<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use App\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

class ParseTennisCommand extends Command
{
    protected static $defaultName = 'app:parse-tennis';
    private $_logger;

    public function __construct(LoggerInterface $_logger = null)
    {
        parent::__construct();
        $this->_logger = $_logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('TRUNCATE table {tennis} & execute all available parse-tennis commands');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // TRUNCATE TABLE {tennis} and {tennis_forks} if prod mode
        if ($_ENV["APP_ENV"] == "prod") {
            try {
                // set database connection
                $conn = &CommandTrait::setDbConnection();
                $sql = "TRUNCATE `tennis`;";
                $res = $conn->prepare($sql);
                $res->execute();
                $this->_logger->info("Table {tennis} truncated!");
                $sql = "TRUNCATE `tennis_forks`;";
                $res = $conn->prepare($sql);
                $res->execute();
                $this->_logger->info("Table {tennis_forks} truncated!");
                $res = null;
                $conn = null;
            } catch (\PDOException $e) {
                $this->_logger->error($e->getMessage());
                die;
            }
        }

        // TRUNCATE TABLE {tennis_forks} if prod mode
        if ($_ENV["APP_ENV"] == "prod") {
            try { } catch (\PDOException $e) {
                $this->_logger->error($e->getMessage());
                die;
            }
        }
        // parse unibet.com bookmaker in new process
        $processUnibetTennis1 = new \swoole_process(function ($processUnibetTennis1) use ($output) {
            $command = $this->getApplication()->find('app:unibet-tennis-winner-ou-games');
            $arguments = [
                'command' => 'unibet-tennis-winner-ou-games'
            ];
            $inputArray = new ArrayInput($arguments);
            $command->run($inputArray, $output);
        }, false, true);
        if ($processUnibetTennis1->start() == false) {
            $this->_logger->error('Can not start parse unibet.com tennis winner & ou games process!');
        } else {
            \swoole_process::wait(false);
        }

        $processUnibetTennis2 = new \swoole_process(function ($processUnibetTennis2) use ($output) {
            $command = $this->getApplication()->find('app:unibet-tennis-ou-sets');
            $arguments = [
                'command' => 'unibet-tennis-ou-sets'
            ];
            $inputArray = new ArrayInput($arguments);
            $command->run($inputArray, $output);
        }, false, true);
        if ($processUnibetTennis2->start() == false) {
            $this->_logger->error('Can not start parse unibet.com tennis ou sets process!');
        } else {
            \swoole_process::wait(false);
        }

        // parse meridianbet.me bookmaker in new process
        $processMeridianbetTennis = new \swoole_process(function ($processMeridianbetTennis) use ($output) {
            $command = $this->getApplication()->find('app:meridianbetME-tennis');
            $arguments = [
                'command' => 'meridianbetME-tennis'
            ];
            $inputArray = new ArrayInput($arguments);
            $command->run($inputArray, $output);
        }, false, true);
        if ($processMeridianbetTennis->start() == false) {
            $this->_logger->error('Can not start parse meridianbet.me tennis process!');
        } else {
            \swoole_process::wait(false);
        }

        /* 
        // parse olimpwin.com (Montenegro - ME) bookmaker
        // save only 1 file from olimpwin.com, they put all in one.
        $command = $this->getApplication()->find('app:olimpwinME-tennis-winner');
        $arguments = [
            'command' => 'olimpwinME-tennis-winner'
        ];
        $inputArray = new ArrayInput($arguments);
        $command->run($inputArray, $output);
*/
    }
}
