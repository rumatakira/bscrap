<?php

/**
 * save only 1 file from olimpwin.com, they put all in one.
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DomCrawler\Crawler;

class OlimpwinMETennisWinnerCommand extends Command
{
    protected static $defaultName = 'app:olimpwinME-tennis-winner';

    protected function configure()
    {
        $this
            ->setDescription('Parse & add to db olimpwin.com (Montenegro - ME) tennis winner quotes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $dataDir = getcwd() . '/data/tennis';

        $io->note(sprintf('Data source: %s', $dataDir));

        // find all files in the current directory
        // files names must be like olimpwinME-tennis-01.html
        $finder = new Finder();
        $finder->files()->name("/olimpwinME-tennis.+\.html$/")->in($dataDir)->depth('== 0');

        // check if there are any search results
        if ($finder->hasResults()) {

            // set database connection
            try {
                $conn = new \PDO("mysql:host=" . $_ENV["DB_HOST"] . ";" . "dbname=" . $_ENV["DB_NAME"], $_ENV["DB_USER"], $_ENV["DB_PASS"]);
            } catch (\PDOException $e) {
                $io->warning($e->getMessage());
                die;
            }

            // finding and count data files
            $countOfFiles = iterator_count($finder->files());
            $progressBarStep = 100 / $countOfFiles;
            $io->title("Found next data files ($countOfFiles):");
            foreach ($finder as $file) {
                $fileNameWithExtension = $file->getRelativePathname();
                $io->success($fileNameWithExtension);
            }

            // show progress bar
            $io->section('Parsing the files');
            $progressBarStep = 100 / $countOfFiles;
            $io->progressStart(100);
            $io->newLine();

            // parse the files
            foreach ($finder as $file) {
                $contents = $file->getContents();
                $crawler = new Crawler();
                $crawler->addHtmlContent($contents);

                // get participants
                $domParticipants = $crawler->filterXPath('//div[@class="liga 6"]//td[@class="parPar extraTips"]')->extract(['_text']);

                // get odds
                $odds = $crawler->filterXPath('//div[@class="liga 6"]//td[@class="tgp"]')->extract(['_text']);

                // explode participants
                $participants = [];
                foreach ($domParticipants as $string) {
                    $parts = explode(' - ', $string);
                    $participants[] = $parts[0];
                    $participants[] = $parts[1];
                }
                // replace ',' to '.' for float
                $odds = str_replace(',', '.', $odds);

                // verify does arrays has same dimentions
                if (count($participants) !== count($odds)) {
                    $io->warning('Participants and odds arrays has not same dimension!');
                    die;
                }

                //insetrt into database 
                try {
                    for ($i = 0, $size = count($participants); $i < $size; ++$i) {
                        $host = $participants[$i];
                        $hostOdd = floatval($odds[$i]);
                        ++$i;
                        $away = $participants[$i];
                        $awayOdd = floatval($odds[$i]);
                        $sql = "INSERT INTO tennis (bookmaker, host, host_odd_under_odd, away, away_odd_over_odd, game_type) VALUES (?,?,?,?,?,?)";
                        $res = $conn->prepare($sql);
                        // game_type = 'winner'
                        $res->execute(['olimpwinME', $host, $hostOdd, $away, $awayOdd, 'winner']);
                    }
                } catch (\PDOException $e) {
                    $io->warning($e->getMessage());
                    die;
                }
                $fileNameWithExtension = $file->getRelativePathname();
                $io->newLine();
                $io->newLine();
                $rows = count($participants) / 2;
                $io->success("$rows rows for $fileNameWithExtension saved to database!");
                $io->newLine();
                $io->progressAdvance($progressBarStep);
            }

            // remove duplicate rows
            try {
                $sql = "DELETE n1 FROM tennis n1, tennis n2
                 WHERE n1.id > n2.id
                    AND n1.host = n2.host
                    AND n1.away = n2.away
                    AND n1.bookmaker = n2.bookmaker;";
                $res = $conn->prepare($sql);
                $res->execute();
                $count = $res->rowCount();
                // show success
                $io->newLine();
                $io->newLine();
                $io->success("$count duplicated rows removed from database!");
            } catch (\PDOException $e) {
                $io->warning($e->getMessage());
                die;
            }
            $io->progressFinish();
        } else {
            $io->warning('No today data files found!');
            die;
        }
    }
}
