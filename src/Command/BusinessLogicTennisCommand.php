<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use App\Entity\GameTypesTennis;


class BusinessLogicTennisCommand extends Command
{
    protected static $defaultName = "app:business-logic-tennis";
    protected $forkRows = 0;

    protected function configure()
    {
        $this
            ->setDescription("Analizing table {tennis}, find forks & save to db table {forks-tennis}." . "\n" .  "Option: minimum profit percentage (aka --percentage=2.2), default - 2%.")
            ->addOption("percentage", null, InputOption::VALUE_OPTIONAL, "Minimum profit percentage (aka --percentage=2.2), default - 2%", 2);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->note("Analizing table {tennis}, find forks & save to db table {forks-tennis}.");
        $io->note("Option: minimum profit percentage (aka --percentage=2.2), default - 2%.");

        // set database connection
        $conn = &CommandTrait::setDbConnection();

        $sql = "SELECT COUNT(*) FROM tennis";
        if ($count = $conn->query($sql)) {

            // if not empty {tennis} table
            if ($count->fetchColumn() > 0) {

                // for each of tennis game types
                foreach (GameTypesTennis::GT_TENNIS as $gameType) {

                    // get all rows and work with each
                    $sql = "SELECT * FROM tennis ORDER BY host ASC";
                    foreach ($conn->query($sql) as $row) {
                        $host =  $row["host"];
                        $away = $row["away"];

                        // find duplicates
                        $sql = "SELECT * FROM tennis 
                                    WHERE host LIKE ?
                                    AND away LIKE ?
                                    AND game_type = ?
                                    AND analized = 0 ;";
                        $duplicates = $conn->prepare($sql);
                        $duplicates->execute(array("%$host%", "%$away%", $gameType));
                        $result = $duplicates->fetchAll();
                        $size = count($result);

                        // if has 2 or more entities
                        if ($size > 1) {
                            $tmpArr = [];

                            // find max qoutes
                            for ($i = 0, $size; $i < $size; ++$i) {
                                $tmpArr[$i]["id"] = $result[$i]["id"];
                                $tmpArr[$i]["host_odd_under_odd"] = floatval($result[$i]["host_odd_under_odd"]);
                                $tmpArr[$i]["away_odd_over_odd"] = floatval($result[$i]["away_odd_over_odd"]);
                                $tmpArr[$i]["game_type"] = $result[$i]["game_type"];
                            }
                            $maxOddHost = $this->maxValueInArray($tmpArr, "host_odd_under_odd");
                            $maxOddAway = $this->maxValueInArray($tmpArr, "away_odd_over_odd");

                            // is a fork?
                            if ($maxOddHost[1] !== $maxOddAway[1]) {
                                $fork = (1 / $maxOddHost[0] + 1 / $maxOddAway[0]);
                                if ($fork < 1) {

                                    // fork in float
                                    $floatFork = round($fork, 3, PHP_ROUND_HALF_DOWN);

                                    // fork profit percentage
                                    $percentageFork = 100 - ($floatFork * 100);
                                    $percentageFork = round($percentageFork, 2, PHP_ROUND_HALF_DOWN);
                                    if ($percentageFork >= $input->getOption("percentage")) {

                                        // prepare for DB array
                                        $_forDbArray = [];
                                        $_forDbArray["floatFork"] = $floatFork;
                                        $_forDbArray["percentageFork"] = $percentageFork;

                                        // host and away id"s
                                        $_forDbArray["hostId"] = $maxOddHost[1];
                                        $_forDbArray["awayId"] = $maxOddAway[1];

                                        // save fork to database
                                        $this->saveForkToDatabase($_forDbArray, $result, $conn, $io);
                                    }
                                }
                            }
                        }
                    }
                }

                // success forks saving to the table {tennis_forks}
                $forkRows = $this->forkRows;
                $io->newLine();
                $io->success("$forkRows forks saved to {tennis_forks} table of database!");

                // if {tennis} table is empty
            } else {
                $io->warning("Empty tennis table !");
            }
        }
    }

    protected function maxValueInArray(&$array, $keyToSearch)
    {
        $currentMax = null;
        $id = null;
        foreach ($array as $arr) {
            foreach ($arr as $key => $value) {
                if ($key == $keyToSearch && ($value >= $currentMax)) {
                    $currentMax = $value;
                    $id = intval($arr["id"]);
                }
            }
        }
        return [$currentMax, $id];
    }

    protected function saveForkToDatabase(&$forkArray, &$queryResult, &$connection, &$io)
    {
        // prepare data to insert to {tennis_forks} table
        $data = [];
        foreach ($queryResult as $arr) {
            if (intval($arr["id"]) == intval($forkArray["hostId"])) {
                $data["host_id"] = intval($forkArray["hostId"]);
                $data["host_bookmaker"] = $arr["bookmaker"];
                $data["host"] = $arr["host"];
                $data["host_odd_under_odd"] = floatval($arr["host_odd_under_odd"]);
            }
        }

        foreach ($queryResult as $arr) {
            if (intval($arr["id"]) == intval($forkArray["awayId"])) {
                $data["away_id"] = intval($forkArray["awayId"]);
                $data["away_bookmaker"] = $arr["bookmaker"];
                $data["away"] = $arr["away"];
                $data["away_odd_over_odd"] = floatval($arr["away_odd_over_odd"]);
                $data["game_type"] = $arr["game_type"];
            }
        }
        $data["float_fork"] = $forkArray["floatFork"];
        $data["percentage_fork"] = $forkArray["percentageFork"];

        // save fork to {tennis_forks} table
        try {
            $sql = "INSERT INTO tennis_forks (host_id, host_bookmaker, host, host_odd_under_odd, away_id, away_bookmaker, away, away_odd_over_odd, game_type, float_fork, percentage_fork) VALUES (:host_id, :host_bookmaker, :host, :host_odd_under_odd, :away_id, :away_bookmaker, :away, :away_odd_over_odd, :game_type, :float_fork, :percentage_fork )";
            $query = $connection->prepare($sql);
            $query->execute($data);
        } catch (\PDOException $e) {
            $io->warning($e->getMessage());
            die;
        }

        // set [analized] field to 1
        try {
            foreach ($queryResult as $arr) {
                $id = intval($arr["id"]);
                $sql = "UPDATE tennis 
                        SET 
                            analized = 1
                        WHERE id = ?;";
                $query = $connection->prepare($sql);
                $query->execute(array($id));
            }
        } catch (\PDOException $e) {
            $io->warning($e->getMessage());
            die;
        }
        $this->forkRows += $query->rowCount();
    }
}
