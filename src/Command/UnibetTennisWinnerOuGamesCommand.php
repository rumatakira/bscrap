<?php

/** @author Kirill A.Lapchinsky rumatakira74@gmail.com
 *  @copyright 2019 Kirill A.Lapchinsky All Rights Reserved
 */

namespace App\Command;

use Psr\Log\LoggerInterface;
use App\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Parse & add to DB from unibet.com tennis winner and over-under total games quotes.
 */
class UnibetTennisWinnerOuGamesCommand extends Command
{
    protected static $defaultName = 'app:unibet-tennis-winner-ou-games';

    // for DB array creation
    private $_forDbArray = [];
    private $_parentIterator = 0;
    private $_logger;

    public function __construct(LoggerInterface $_logger = null)
    {
        parent::__construct();
        $this->_logger = $_logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse & add to DB from unibet.com tennis winner and over-under total games quotes');
    }
    //end configure()

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $dataDir = getcwd() . '/data/tennis';


        // find file in the current directory
        // file name must be unibet-tennis-01.html
        $finder = new Finder();
        $finder->files()->name("/unibet-tennis-01.html$/")->in(strval($dataDir))->depth('== 0');

        // check if there are any search results
        if ($finder->hasResults()) {

            // set database connection
            $conn = &CommandTrait::setDbConnection();
            $contents = null;

            // finding and count data files
            $countOfFiles = iterator_count($finder->files());
            foreach ($finder as $file) {
                $fileNameWithExtension = $file->getRelativePathname();
                global $contents;
                $contents = $file->getContents();
            }

            // parse the file
            $crawler = new Crawler();
            $crawler->addHtmlContent(strval($contents));

            // get events parent Crawler
            $events = $crawler->filterXPath('//div[@class="KambiBC-collapsible-container KambiBC-mod-event-group-container KambiBC-expanded"]');

            $events->each(
                function (Crawler $parentCrawler, $i) {
                    $this->_parentIterator = $i;
                    // extract unibet events date
                    $eventDate = $parentCrawler->filterXPath('//span[@class="KambiBC-mod-event-group-header__details"]')->extract(['_text']);
                    switch ($eventDate[0]) {
                        case "Today":
                            $date = date('Y-m-d');
                            break;
                        case "Tomorrow":
                            $date = (date('Y-m-d', strtotime("+1 day")));
                            break;
                        default:
                            $date = strtotime($eventDate[0] . ' ' . date('Y'));
                            $date = date("Y-m-d", intval($date));
                    }
                    $this->_forDbArray[$this->_parentIterator]['events_date'] = $date;

                    $parentCrawler->each(
                        function (Crawler $parentCrawler, $i) {

                            // new parent event-wrapper
                            $eventItems = $parentCrawler->filterXPath('//div[@class="KambiBC-event-item__event-wrapper"]');
                            $eventItems->each(
                                function (Crawler $parentCrawler, $i) {

                                    // extract event time
                                    $eventTimeArray = $parentCrawler->filterXPath('//span[@class="KambiBC-event-item__start-time--time"]')->extract(['_text']);
                                    $this->_forDbArray[$this->_parentIterator]['events'][$i]['event_time'] = $eventTimeArray;

                                    // extract participants
                                    $participants = $parentCrawler->filterXPath('//div[@class="KambiBC-event-participants__name"]')->extract(['_text']);

                                    //extract winner odds
                                    $game = $parentCrawler->filterXPath('//div[@class = "KambiBC-bet-offer KambiBC-bet-offer--onecrosstwo KambiBC-bet-offer--inline KambiBC-bet-offer--outcomes-2"]');
                                    $odds = $game->filterXPath('//span[@class="KambiBC-mod-outcome__odds"]')->extract(['_text']);

                                    $this->_forDbArray[$this->_parentIterator]['events'][$i]['participants'] = $participants;

                                    if (!empty($odds)) {
                                        $this->_forDbArray[$this->_parentIterator]['events'][$i]['winner']['odds'] = $odds;
                                    }

                                    // extract over-under line and odds
                                    $game = $parentCrawler->filterXPath('//div[@class="KambiBC-bet-offer KambiBC-bet-offer--overunder KambiBC-bet-offer--inline KambiBC-bet-offer--outcomes-2"]');
                                    $line = $game->filterXPath('//span[@class="KambiBC-mod-outcome__line"]')->extract(['_text']);
                                    $odds = $game->filterXPath('//span[@class="KambiBC-mod-outcome__odds"]')->extract(['_text']);

                                    if (!empty($line) and !empty($odds)) {

                                        $this->_forDbArray[$this->_parentIterator]['events'][$i]['over_under']['line'] = $line[0];
                                        $this->_forDbArray[$this->_parentIterator]['events'][$i]['over_under']['odds'] = $odds;
                                    }
                                }
                            );
                        }
                    );
                }
            );

            // insetrt into database 
            $this->insertWinner($conn);
            $this->insertOverUnder($conn);
            //eval(\Psy\sh());
        } else {
            $this->_logger->error('No unibet-tennis-01.html file found!');
        };
    }
    //end execute()

    /**
     * Insert winner quotes into database.
     *
     * @param reference $connection - \PDO connection
     * 
     * @return void
     */
    private function insertWinner(\PDO &$connection): void
    {
        $rowsIterator = 0;
        $date = $time = $hostParticipant = $awayParticipant = $hostWinnerOdd = $awayWinnerOdd = null;
        // prepare winner data to insert to {tennis} table
        try {
            foreach ($this->_forDbArray as $day) {
                $date = $day['events_date'];
                if (!empty($day['events'])) {
                    foreach ($day['events'] as $event) {
                        if (!empty($event['winner'])) {
                            try {
                                if (!empty($event['event_time'])) {
                                    $time = $event['event_time'][0];
                                }
                                $hostParticipant = trim($event['participants'][0]);
                                $hostParticipant = str_replace([' ', ',', '-', '.'], '', $hostParticipant);
                                $hostWinnerOdd = floatval($event['winner']['odds'][0]);
                                $awayParticipant = trim($event['participants'][1]);
                                $awayParticipant = str_replace([' ', ',', '-', '.'], '', $awayParticipant);
                                $awayWinnerOdd = floatval($event['winner']['odds'][1]);

                                $data = [
                                    'event_date' => $date,
                                    'event_time' => $time,
                                    'bookmaker' => 'unibet.com',
                                    'host' => $hostParticipant,
                                    'host_odd_under_odd' => $hostWinnerOdd,
                                    'away' => $awayParticipant,
                                    'away_odd_over_odd' => $awayWinnerOdd,
                                    'game_type' => 'winner',
                                ];
                                $sql = "INSERT INTO tennis (event_date, event_time, bookmaker, host, host_odd_under_odd, away, away_odd_over_odd, game_type) VALUES (:event_date, :event_time, :bookmaker, :host, :host_odd_under_odd, :away, :away_odd_over_odd, :game_type)";
                                $resPrepare = $connection->prepare($sql);
                                $resInsert = $resPrepare->execute($data);
                                ++$rowsIterator;
                            } catch (\PDOException $e) {
                                $this->_logger->error($e->getMessage());
                            }
                        }
                    }
                }
            }

            // log insert success
            $this->_logger->info("$rowsIterator rows for  unibet.com/tennis, game type 'winner', saved to database!");

            // remove duplicate rows
            $sql = "DELETE n1 FROM tennis n1, tennis n2
             WHERE n1.id > n2.id
                AND n1.host = n2.host
                AND n1.away = n2.away
                AND n1.bookmaker = 'unibet.com'
                AND n2.bookmaker = 'unibet.com'
                AND n1.game_type = 'winner'
                AND n2.game_type = 'winner';";
            $res = $connection->prepare($sql);
            $res->execute();
            $count = $res->rowCount();

            // log duplicated rows remove success
            if ($count > 0) {
                $this->_logger->info("$count duplicated rows for unibet.com/tennis, game type 'winner', removed from database!");
            }
        } catch (\PDOException $e) {
            $this->_logger->error($e->getMessage());
        }
    }
    //end insertWinner()

    /**
     * Insert over/under total games quotes into database.
     *
     * @param reference $connection - \PDO connection
     * 
     * @return void
     */
    private function insertOverUnder(\PDO &$connection): void
    {
        $rowsIterator = 0;
        $date = $time = $hostParticipant = $awayParticipant = $overOuGamesOdd = $OuGamesLine = null;
        // prepare over/under total games data to insert to {tennis} table
        try {
            foreach ($this->_forDbArray as $day) {
                $date = $day['events_date'];
                if (!empty($day['events'])) {
                    foreach ($day['events'] as $event) {
                        if (!empty($event['over_under'])) {
                            try {
                                if (!empty($event['event_time'])) {
                                    $time = $event['event_time'][0];
                                }
                                $hostParticipant = trim($event['participants'][0]);
                                $hostParticipant = str_replace([' ', ',', '-', '.'], '', $hostParticipant);
                                $overOuGamesOdd = floatval($event['over_under']['odds'][0]);
                                $awayParticipant = trim($event['participants'][1]);
                                $awayParticipant = str_replace([' ', ',', '-', '.'], '', $awayParticipant);
                                $underOuGamesOdd = floatval($event['over_under']['odds'][1]);
                                $OuGamesLine = floatval($event['over_under']['line']);

                                $data = [
                                    'event_date' => $date,
                                    'event_time' => $time,
                                    'bookmaker' => 'unibet.com',
                                    'host' => $hostParticipant,
                                    'host_odd_under_odd' => $overOuGamesOdd,
                                    'away' => $awayParticipant,
                                    'away_odd_over_odd' => $underOuGamesOdd,
                                    'game_type' => 'over_under_games',
                                    'ou_line' => $OuGamesLine,
                                ];
                                $sql = "INSERT INTO tennis (event_date, event_time, bookmaker, host, host_odd_under_odd, away, away_odd_over_odd, game_type, ou_line) VALUES (:event_date, :event_time, :bookmaker, :host, :host_odd_under_odd, :away, :away_odd_over_odd, :game_type, :ou_line)";
                                $resPrepare = $connection->prepare($sql);
                                $resInsert = $resPrepare->execute($data);
                                ++$rowsIterator;
                            } catch (\PDOException $e) {
                                $this->_logger->error($e->getMessage());
                            }
                        }
                    }
                }
            }

            // log insert success
            $this->_logger->info("$rowsIterator rows for unibet.com/tennis, game type 'over/under total games', saved to database!");

            // remove duplicate rows
            $sql = "DELETE n1 FROM tennis n1, tennis n2
             WHERE n1.id > n2.id
                AND n1.host = n2.host
                AND n1.away = n2.away
                AND n1.bookmaker = 'unibet.com'
                AND n2.bookmaker = 'unibet.com'
                AND n1.game_type = 'over_under_games'
                AND n2.game_type = 'over_under_games'
                AND n1.ou_line = n2.ou_line;";
            $res = $connection->prepare($sql);
            $res->execute();
            $count = $res->rowCount();

            // log duplicated rows remove success
            if ($count > 0) {
                $this->_logger->info("$count duplicated rows for unibet.com/tennis, game type 'over/under total games', removed from database!");
            }
        } catch (\PDOException $e) {
            $this->_logger->error($e->getMessage());
        }
    }
    //end insertOverUnder()
}
//end class
