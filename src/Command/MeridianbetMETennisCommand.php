<?php

/** @author Kirill A.Lapchinsky rumatakira74@gmail.com
 *  @copyright 2019 Kirill A.Lapchinsky All Rights Reserved
 */

namespace App\Command;

use Psr\Log\LoggerInterface;
use App\Command\CommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DomCrawler\Crawler;


/**
 * Parse & add to DB from meridianbet.me tennis quotes.
 */
class MeridianbetMETennisCommand extends Command
{
    protected static $defaultName = 'app:meridianbetME-tennis';

    // for DB array creation
    private $_forDbArray = [];
    private $_parentIterator = 0;
    private $_logger;

    public function __construct(LoggerInterface $_logger = null)
    {
        parent::__construct();
        $this->_logger = $_logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse & add to db meridianbet.me (Montenegro - ME) tennis quotes');
    }
    //end configure()

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dataDir = getcwd() . '/data/tennis';

        // find all files in the current directory
        // files names must be like meridianbetME-tennis-01.html  meridianbetME-tennis-02.html and so on
        $finder = new Finder();
        $finder->files()->name("/meridianbetME-tennis.+\.html$/")->in(strval($dataDir))->depth('== 0');

        // check if there are any search results
        if ($finder->hasResults()) {

            // set database connection
            $conn = &CommandTrait::setDbConnection();

            // finding and count data files
            $countOfFiles = iterator_count($finder->files());
            foreach ($finder as $file) {
                $fileNameWithExtension = $file->getRelativePathname();
            }

            // parse the files
            foreach ($finder as $file) {
                $contents = $file->getContents();
                $crawler = new Crawler();
                $crawler->addHtmlContent(strval($contents));

                // get events parent Crawler
                $events = $crawler->filterXPath('//div[@id="league"]');

                $events->each(
                    function (Crawler $parentCrawler, $i) {

                        // new parent event-wrapper
                        $eventItems = $parentCrawler->filterXPath('.//div[@class="standard match"]');
                        $eventItems->each(
                            function (Crawler $parentCrawler, $i) {

                                //get date
                                $eventDate = $parentCrawler->filterXPath('//div[@class="date"]')->extract(['_text']);

                                $eventTime = $parentCrawler->filterXPath('//div[@class="standard match"]//div[@class="time"]')->extract(['_text']);

                                // get participants
                                $hostParticipant = $parentCrawler->filterXPath('//div[@class="standard match"]//div[@class="home serving"]')->extract(['_text']);

                                $awayParticipant = $parentCrawler->filterXPath('//div[@class="standard match"]//div[@class="away"]')->extract(['_text']);

                                // get winner odds
                                $hostWinnerOdd = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" Winner "]
                            /..//div[@class="selection-name" and text()="1"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);

                                $awayWinnerOdd = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" Winner "]
                            /..//div[@class="selection-name" and text()="2"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);

                                // get o/u total games odds
                                $overOuGamesOdd = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" Total games "]
                            /..//div[@class="selection-name" and text()="Over"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);

                                $underOuGamesOdd = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" Total games "]
                            /..//div[@class="selection-name" and text()="Under"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);

                                $OuGamesLine = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" Total games "]
                            /..//div[@class="selection-name" and text()="OU"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);

                                // get 1 Set-Winner odds
                                $hostFirstSetWinnerOdd = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" 1 Set-Winner "]
                            /..//div[@class="selection-name" and text()="1"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);

                                $awayFirstSetWinnerOdd = $parentCrawler->filterXPath('
                            //div[@class="game-name tooltip" and text()=" 1 Set-Winner "]
                            /..//div[@class="selection-name" and text()="2"]
                            /..//div[@class="selection-odd"]
                        ')->extract(['_text']);
                                if (!empty($eventDate)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['eventDate'] = $eventDate[0];
                                }
                                if (!empty($eventTime)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['eventTime'] = $eventTime[0];
                                }
                                if (!empty($hostParticipant)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['hostParticipant'] = $hostParticipant[0];
                                }
                                if (!empty($awayParticipant)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['awayParticipant'] = $awayParticipant[0];
                                }
                                if (!empty($hostWinnerOdd)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['hostWinnerOdd'] = $hostWinnerOdd[0];
                                }
                                if (!empty($awayWinnerOdd)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['awayWinnerOdd'] = $awayWinnerOdd[0];
                                }
                                if (!empty($overOuGamesOdd)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['overOuGamesOdd'] = $overOuGamesOdd[0];
                                }
                                if (!empty($underOuGamesOdd)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['underOuGamesOdd'] = $underOuGamesOdd[0];
                                }
                                if (!empty($OuGamesLine)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['OuGamesLine'] = $OuGamesLine[0];
                                }
                                if (!empty($hostFirstSetWinnerOdd)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['hostFirstSetWinnerOdd'] = $hostFirstSetWinnerOdd[0];
                                }
                                if (!empty($awayFirstSetWinnerOdd)) {
                                    $this->_forDbArray[$this->_parentIterator][$i]['awayFirstSetWinnerOdd'] = $awayFirstSetWinnerOdd[0];
                                }
                            }
                        );
                    }
                );
                $this->_parentIterator = ++$this->_parentIterator;
            }
            //eval(\Psy\sh());
            // insetrt into database 
            $this->insertIntoDb($conn);
        } else {
            $this->_logger->error('No data files found for meridianbet.me/tennis!');
        }
    }
    //end execute()

    /**
     * Insert all meribianbet.me quotes into database.
     *
     * @param reference $connection - \PDO connection
     * 
     * @return void
     */
    private function insertIntoDb(\PDO &$connection): void
    {
        $rowsIterator = 0;
        $count = 0;
        $date = $time = $hostParticipant = $awayParticipant = $hostWinnerOdd = $awayWinnerOdd = $overOuGamesOdd = $OuGamesLine = $hostFirstSetWinnerOdd = $awayFirstSetWinnerOdd = null;


        // prepare data to insert to {tennis} table
        foreach ($this->_forDbArray as $file) {
            foreach ($file as $event) {
                if (!empty($event['eventDate'])) {
                    $date = $event['eventDate'];
                    $date = strtotime($date . date('Y'));
                    $date = date("Y-m-d", intval($date));
                }
                if (!empty($event['eventTime'])) {
                    $time = $event['eventTime'];
                }
                if (!empty($event['hostParticipant'])) {
                    $hostParticipant = trim($event['hostParticipant']);
                    $hostParticipant = str_replace([' ', ',', '-', '.'], '', $hostParticipant);
                }
                if (!empty($event['awayParticipant'])) {
                    $awayParticipant = trim($event['awayParticipant']);
                    $awayParticipant = str_replace([' ', ',', '-', '.'], '', $awayParticipant);
                }
                if (!empty($event['hostWinnerOdd'])) {
                    $hostWinnerOdd = floatval($event['hostWinnerOdd']);
                }
                if (!empty($event['awayWinnerOdd'])) {
                    $awayWinnerOdd = floatval($event['awayWinnerOdd']);
                }
                if (!empty($event['overOuGamesOdd'])) {
                    $overOuGamesOdd = floatval($event['overOuGamesOdd']);
                }
                if (!empty($event['underOuGamesOdd'])) {
                    $underOuGamesOdd = floatval($event['underOuGamesOdd']);
                }
                if (!empty($event['OuGamesLine'])) {
                    $OuGamesLine = floatval($event['OuGamesLine']);
                }
                if (!empty($event['hostFirstSetWinnerOdd'])) {
                    $hostFirstSetWinnerOdd = floatval($event['hostFirstSetWinnerOdd']);
                }
                if (!empty($event['awayFirstSetWinnerOdd'])) {
                    $awayFirstSetWinnerOdd = floatval($event['awayFirstSetWinnerOdd']);
                }

                // inset winner game odds
                if (!empty($hostParticipant) and !empty($awayParticipant) and !empty($hostWinnerOdd) and !empty($awayWinnerOdd)) {
                    try {
                        $data = [
                            'event_date' => $date,
                            'event_time' => $time,
                            'bookmaker' => 'meridianbet.me',
                            'host' => $hostParticipant,
                            'host_odd_under_odd' => $hostWinnerOdd,
                            'away' => $awayParticipant,
                            'away_odd_over_odd' => $awayWinnerOdd,
                            'game_type' => 'winner',
                        ];
                        $sql = "INSERT INTO tennis (event_date, event_time, bookmaker, host, host_odd_under_odd, away, away_odd_over_odd, game_type) VALUES (:event_date, :event_time, :bookmaker, :host, :host_odd_under_odd, :away, :away_odd_over_odd, :game_type)";
                        $resPrepare = $connection->prepare($sql);
                        $resInsert = $resPrepare->execute($data);
                        ++$rowsIterator;
                    } catch (\PDOException $e) {
                        $this->_logger->error($e->getMessage());
                    }
                }

                // insert ou games odds
                if (!empty($hostParticipant) and !empty($awayParticipant) and !empty($overOuGamesOdd) and !empty($underOuGamesOdd) and !empty($OuGamesLine)) {
                    try {
                        $data = [
                            'event_date' => $date,
                            'event_time' => $time,
                            'bookmaker' => 'meridianbet.me',
                            'host' => $hostParticipant,
                            'host_odd_under_odd' => $overOuGamesOdd,
                            'away' => $awayParticipant,
                            'away_odd_over_odd' => $underOuGamesOdd,
                            'game_type' => 'over_under_games',
                            'ou_line' => $OuGamesLine,
                        ];
                        $sql = "INSERT INTO tennis (event_date, event_time, bookmaker, host, host_odd_under_odd, away, away_odd_over_odd, game_type, ou_line) VALUES (:event_date, :event_time, :bookmaker, :host, :host_odd_under_odd, :away, :away_odd_over_odd, :game_type, :ou_line)";
                        $resPrepare = $connection->prepare($sql);
                        $resInsert = $resPrepare->execute($data);
                        ++$rowsIterator;
                    } catch (\PDOException $e) {
                        $this->_logger->error($e->getMessage());
                    }
                }

                // insert first set winner odds
                if (!empty($hostParticipant) and !empty($awayParticipant) and !empty($hostFirstSetWinnerOdd) and !empty($awayFirstSetWinnerOdd)) {
                    try {
                        $data = [
                            'event_date' => $date,
                            'event_time' => $time,
                            'bookmaker' => 'meridianbet.me',
                            'host' => $hostParticipant,
                            'host_odd_under_odd' => $hostFirstSetWinnerOdd,
                            'away' => $awayParticipant,
                            'away_odd_over_odd' => $awayFirstSetWinnerOdd,
                            'game_type' => 'first_set_winner',
                        ];
                        $sql = "INSERT INTO tennis (event_date, event_time, bookmaker, host, host_odd_under_odd, away, away_odd_over_odd, game_type) VALUES (:event_date, :event_time, :bookmaker, :host, :host_odd_under_odd, :away, :away_odd_over_odd, :game_type)";
                        $resPrepare = $connection->prepare($sql);
                        $resInsert = $resPrepare->execute($data);
                        ++$rowsIterator;
                    } catch (\PDOException $e) {
                        $this->_logger->error($e->getMessage());
                    }
                }
            }
        }

        // log success
        $this->_logger->info("$rowsIterator rows for meridianbet.me/tennis, saved to database!");

        // remove duplicate rows
        $sqlWinner = "DELETE n1 FROM tennis n1, tennis n2
            WHERE n1.id > n2.id
            AND n1.host = n2.host
            AND n1.away = n2.away
            AND n1.bookmaker = 'meridianbet.me'
            AND n2.bookmaker = 'meridianbet.me'
            AND n1.game_type = 'winner'
            AND n2.game_type = 'winner';";

        $sqlOuGames = "DELETE n1 FROM tennis n1, tennis n2
            WHERE n1.id > n2.id
            AND n1.host = n2.host
            AND n1.away = n2.away
            AND n1.bookmaker = 'meridianbet.me'
            AND n2.bookmaker = 'meridianbet.me'
            AND n1.game_type = 'over_under_games'
            AND n2.game_type = 'over_under_games';";

        $sqlFirstSetWinner = "DELETE n1 FROM tennis n1, tennis n2
            WHERE n1.id > n2.id
            AND n1.host = n2.host
            AND n1.away = n2.away
            AND n1.bookmaker = 'meridianbet.me'
            AND n2.bookmaker = 'meridianbet.me'
            AND n1.game_type = 'first_set_winner'
            AND n2.game_type = 'first_set_winner';";

        try {
            $res = $connection->prepare($sqlWinner);
            $res->execute();
            $count = $count + $res->rowCount();

            $res = $connection->prepare($sqlOuGames);
            $res->execute();
            $count = $count + $res->rowCount();

            $res = $connection->prepare($sqlFirstSetWinner);
            $res->execute();
            $count = $count + $res->rowCount();

            // log success
            if ($count > 0) {
                $this->_logger->info("$count duplicated rows for meridianbet.me/tennis, removed from database!");
            }
        } catch (\PDOException $e) {
            $this->_logger->error($e->getMessage());
        }
    }
}
//end class
