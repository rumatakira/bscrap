<?php

/** @author Kirill A.Lapchinsky rumatakira74@gmail.com
 *  @copyright 2019 Kirill A.Lapchinsky All Rights Reserved
 */

namespace App\Command;

/**
 * Implements comands trait used by console application.
 */
trait CommandTrait
{
    /**
     * Set database connection.
     *
     * @return reference $connection - \PDO connection
     */
    public static function &setDbConnection()
    {
        try {
            $connection = new \PDO("mysql:host=" . $_ENV["DB_HOST"] . ";" . "dbname=" . $_ENV["DB_NAME"], $_ENV["DB_USER"], $_ENV["DB_PASS"]);
            return $connection;
        } catch (\PDOException $e) {
            exit(strval($e));
        }
    }
}
